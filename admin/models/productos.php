<?php

Class Productos{

	public  function getProducts(){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM ko_productos order by nombre_producto desc;";
		$user = DB::query($query);
		return $user;
	}

	public  function getProduct( $id_producto ){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM ko_productos where id_producto = $id_producto;";
		$user = DB::query($query);
		return $user[0];
	}

	public  function insertProduct( $p ){
		extract( $p );
		include_once('../config/init_db.php');
		$fecha_creacion = FECHA;
		$query = "INSERT INTO ko_productos
							(
							nombre_producto,
							referencia,
							precio,
							peso,
							categoria,
							stock,
							fecha_creacion
							)
							VALUES
							(
							'$nombre_producto',
							'$referencia',
							'$precio',
							'$peso',
							'$categoria',
							'$stock',
							'$fecha_creacion'
							);";
		$res_query = DB::query($query);

		$data = array();
		if( $res_query ){
			$data['error'] 	= false;
			$data['msj'] 	= "Producto creado correctamente" ;
		}else{
			$data['error'] 	= true;
			$data['msj']	= "No se pudo crear el producto";
		}
		return $data;
	}

	public  function updateProduct( $p ){
		extract( $p );
		//print_r($p); die();
		include_once('../config/init_db.php');
		$fecha_creacion = FECHA;
		$query = "UPDATE ko_productos
						SET
						nombre_producto 	= '$nombre_producto',
						referencia 			= '$referencia',
						precio 				= '$precio',
						peso 				= '$peso',
						categoria 			= '$categoria',
						stock 				= '$stock',
						fecha_creacion 		= '$fecha_creacion'
						WHERE id_producto 	= '$id_producto';";
		$res_query = DB::query($query);

		$data = array();
		if( $res_query ){
			$data['error'] 	= false;
			$data['msj'] 	= "Producto actualizado correctamente";
		}else{
			$data['error'] 	= true;
			$data['msn'] 	= "No se puso actualizar el usuario";
		}
		return $data;
	}

	public  function deletedProduct( $id_producto ){
		include_once('../config/init_db.php');
		$query = "DELETE FROM ko_productos WHERE id_producto = '$id_producto';";
		$res_query = DB::query($query);

		$data = array();
		if( $res_query ){
			$data['error'] 	= false;
			$data['msj'] 	= "Producto eliminado correctamente";
		}else{
			$data['error'] 	= true;
			$data['msj']	= "No se puso eliminar el producto";
		}
		return $data;
	}

	public  function venderProduct( $id_producto ){
		include_once('../config/init_db.php');
		$fecha_ult_venta = FECHA;

		$query = "SELECT stock FROM ko_productos where id_producto = $id_producto and stock > 0;";
		$res_query = DB::queryFirstRow($query);

		$data = array();
		if ( isset( $res_query['stock'] )  ) {
			$query = "UPDATE ko_productos set stock = (stock -1), fecha_ult_venta = '$fecha_ult_venta' where id_producto = $id_producto;";
			$res_query = DB::query($query);

			if( $res_query ){
				$data['error'] 	= false;
				$data['msj'] 	= "Producto vendido correctamente";
			}else{
				$data['error'] 	= true;
				$data['msj']	= "No se puso vender el producto";
			}
		}else{
			$data['error'] 	= true;
			$data['msj']	= "No se puede ejecutar esta acción, este item no cuenta productos en stock";
		}
		
		return $data;
	}

}

