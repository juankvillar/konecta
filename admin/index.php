<?php include('controllers/productos.php'); ?>
<?php include('../template/header.php'); ?>
<div class="container">
  <form method="post" action="index.php">
    <div class="form-group">
      <label for="nombre_producto">Nombre Producto</label>
      <input value="<?php if (isset($_GET['opcn']) and $_GET['opcn'] == "editar") {echo $res_data['nombre_producto'];} ?>" type="text" class="form-control" id="nombre_producto" name="nombre_producto" required>
      <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
    </div>
    <div class="form-group">
      <label for="referencia">Referencia</label>
      <input value="<?php if (isset($_GET['opcn']) and $_GET['opcn'] == "editar") {echo $res_data['referencia'];} ?>" type="referencia" class="form-control" id="referencia" name="referencia" required>
    </div>
    <div class="form-group">
      <label for="precio">Precio</label>
      <input value="<?php if (isset($_GET['opcn']) and $_GET['opcn'] == "editar") {echo $res_data['precio'];} ?>" type="number" class="form-control" id="precio" name="precio" required min="0" max="100000">
    </div>
    <div class="form-group">
      <label for="peso">Peso</label>
      <input value="<?php if (isset($_GET['opcn']) and $_GET['opcn'] == "editar") {echo $res_data['peso'];} ?>" type="number" class="form-control" id="peso" name="peso" required min="0" max="100000">
    </div>
    <div class="form-group">
      <label for="categoria">Categoria</label>
      <input value="<?php if (isset($_GET['opcn']) and $_GET['opcn'] == "editar") {echo $res_data['categoria'];} ?>" type="text" class="form-control" id="categoria" name="categoria" required>
    </div>
    <div class="form-group">
      <label for="stock">Stock</label>
      <input value="<?php if (isset($_GET['opcn']) and $_GET['opcn'] == "editar") {echo $res_data['stock'];} ?>" type="number" class="form-control" id="stock" name="stock" required min="0" max="100000">
    </div>
    <input type="hidden" name="id_producto" value="<?php echo $res_data['id_producto'] ?>">
  
    <?php if( isset( $_GET['opcn']) and $_GET['opcn'] == "editar" ){ ?>
    <button name="opcn" value="editar" type="submit" class="btn btn-warning">Editar</button>
      <a class="btn btn-info" href="productos.php">Volver</a>
    <?php }else{?>
    <button name="opcn" value="crear" type="submit" class="btn btn-primary">Crear</button>
    <?php } ?>
    <div class="text-success"><?php if (isset($res_data['msj'])) {echo $res_data['msj'];} ?></div>
  </form>
  <h2>Productos registrados</h2>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Nombre producto</th>
        <th>Referencia</th>
        <th>Precio</th>
        <th>Peso</th>
        <th>Categoría</th>
        <th>Stock</th>
        <th>Fecha creación</th>
        <th>Fecha última venta</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($productos as $key => $value) { ?>
        <tr>
          <td><?php echo $value['nombre_producto']  ?></td>
          <td><?php echo $value['referencia']  ?></td>
          <td><?php echo $value['precio']  ?></td>
          <td><?php echo $value['peso']  ?></td>
          <td><?php echo $value['categoria']  ?></td>
          <td><?php echo $value['stock']  ?></td>
          <td><?php echo $value['fecha_creacion']  ?></td>
          <td><?php echo $value['fecha_ult_venta']  ?></td>
          <td> <strong>
                <a class="btn btn-warning" href="?opcn=editar&id_producto=<?php echo $value['id_producto'] ?>">Editar</a>
                <a onclick="return confirm('Estás seguro de ejecutar esta acción')" class="btn btn-danger" href="?opcn=eliminar&id_producto=<?php echo $value['id_producto'] ?>">Eliminar</a>
                <a onclick="return confirm('Estás seguro de ejecutar esta acción')" class="btn btn-success" href="?opcn=vender&id_producto=<?php echo $value['id_producto'] ?>">Vender</a>
              </strong>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php include('../template/footer.php'); ?>
<script src="js/productos.js"></script>