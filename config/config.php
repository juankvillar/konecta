<?php

$desarrollo = true;
if ( $desarrollo ) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}
date_default_timezone_set('America/Bogota');
set_time_limit(0);
define('FECHA', date('Y-m-d H:i:s'));
