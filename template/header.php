<!DOCTYPE html>
<html lang="es">
<head>
  <title>Prueba Konecta</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../lib/bootstrap.min.css">
  <link rel="icon" type="image/png" sizes="32x32" href="../resource/img/favicon.png">
  <script src="../lib/jquery.min.js"></script>
  <script src="../lib/bootstrap.min.js"></script>
</head>
<body>